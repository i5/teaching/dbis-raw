
## Docker DBIS UB 2023
- https://docs.docker.com/get-docker/
- https://lerneprogrammieren.de/docker/

**Vorab**: `doku/Neuladen_des_assets-Ordner.ipynb` funktioniert nur im RWTH Jupyter und wird nicht in dieser Umgebung helfen. Dazu empfiehlt es sich, den `notebooks` Ordner zu löschen und den Container neu auszuführen. Dadurch gehen zwar alle Änderungen verloren, aber das Übungs-Repository ist wieder zurückgesetzt. 

Die angegebenen Befehle wurden in der **Windows 10 Powershell** getestet. Linux und Mac user sollten weitestgehend kompatibel sein, ggf. könnten aber Plattform-spezifische anpassungen notwendig sein. Insbesondere ist dies möglicherweise bei `${PWD}` der Fall. 

### Container starten
1. `docker login -u gitlab+deploy-token-970 registry.git.rwth-aachen.de/i5/teaching/dbis-raw`  
  Das Passwort ist `TBAHud6DD4FN9rqyd-yx`
  **Hinweis**:
   - Beim Eintragen des Passworts werden keine Zeichen angezeigt.
   - Einfügen funktioniert normalerweise via Rechtsklick.
2. Erstellen Sie den Ordner `notebooks` und führen Sie den `docker run`-Befehl (siehe 3.) in dem selben Pfad aus.  
3. Nach dem Login kann der Container wie folgt gestartet werden:  
   `docker run --rm -it --pull=always -v ${PWD}/notebooks:/home/jovyan -p 8888:8888 registry.git.rwth-aachen.de/i5/teaching/dbis-raw`  
   - Dieser Befehl erstellt oder benutzt den Ordner `notebooks` relativ vom _working directory_ der Konsole. Dort wird das Git-Repository der Übung heruntergeladen, ähnlich der Jupyter Umgebung. Dieser Ordner kann auf dem Host-System benutzt werden, um Dateien mit der Jupyter-Umgebung auszutauschen.
   - Die Zahl `8888:` vor dem Doppelpunkt gibt den Port auf der Hostmaschine an, die Zahl nach dem Doppelpunkt `:8888` sollte nicht verändert werden, da sie den Port im Jupyter-Container beschreibt.
4. Nachdem der Container gestartet ist, wird in der Konsole ein Link angezeigt.  
   **Beispiel**:
   ```
   http://127.0.0.1:8888/lab?token=e4a75b5baa035c6...
   ```
   Der Link muss nun in den Browser kopiert werden, um die Jupyter Oberfläche auszuführen. In manchen Konsolen kann er auch per `Ctrl+Click` direkt gestartet werden. 
5. Der Container kann via `Ctrl+C` gestoppt werden.

#### (optional) GIT Update
   - Der docker container ist vorkonfiguriert, um automatisch das git-Repository der Übungsmaterialien herunterzuladen. Weitere Schritte sind optional.

   - Mit dem Befehl `docker run --rm -it -v ${PWD}/notebooks:/home/jovyan -p 8888:8888 registry.git.rwth-aachen.de/i5/teaching/dbis-raw update_git.sh` kann das Repository erneut gepulled werden - hierbei können allerdings Merge-Konflikte entstehen, also wird geraten, alle Änderungen anderweitig zu speichern, das Verzeichnis `notebooks/dbis-ss-23` zu entfernen und erst dann den `git_update.sh` Befehl auszuführen.

   -  `git_update.sh` kann auch von der Konsole der Jupyter Umgebung gestartet werden, ebenso wie andere `git` Befehle um externe repositories zu benutzen.

**Hinweis**: *Das Repository mit den Übungsaufgaben hat in diesem Kontext nur die Berechtigung zum Herunterladen und beinhaltet nur die Übungsmaterialien, nie die Musterlösung.*