-- DROP TABLE löscht die Tabelle, damit CREATE TABLE ohne Fehler funktionieren kann
DROP TABLE IF EXISTS countries;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS stores;
DROP TABLE IF EXISTS stores;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS order_items;


/*

	CREATE THE TABLES

*/

CREATE TABLE countries (
  country_id CHAR(2) NOT NULL,
  country_name TEXT NOT NULL,
  -- CHECK bedeutet, dass nur die vorgegebenen Möglichkeiten erlaubt sind
  continent TEXT CHECK( continent IN 
                       ('Africa', 'Asia', 'Europe', 
  						'North America', 'South America', 
  						'Antarctica', 'Australia') 
                      ) NOT NULL,
  PRIMARY KEY (country_id)
);

CREATE TABLE products (
	product_id SERIAL PRIMARY KEY,
	product_name VARCHAR NOT NULL,
	product_price numeric NOT NULL,
    -- CHECK bedeutet, dass nur die vorgegebenen Möglichkeiten erlaubt sind
	product_type CHECK( product_type IN 
                       ('Book', 'Magazine', 'Journal', 'Comic') 
                      ) NOT NULL,
	product_created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE stores (
	store_id SERIAL PRIMARY KEY,
	store_name VARCHAR NOT NULL,
	store_country CHAR(2) NOT NULL REFERENCES countries,
	admin_id INT NOT NULL REFERENCES users,
	store_created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE users (
  user_id SERIAL PRIMARY KEY,
  user_full_name VARCHAR NOT NULL,
  user_created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE orders (
	order_id SERIAL PRIMARY KEY,
	order_store_id INT NOT NULL REFERENCES stores,
	order_user_id INT NOT NULL REFERENCES users,
  -- CHECK bedeutet, dass nur die vorgegebenen Möglichkeiten erlaubt sind
	order_status CHECK( order_status IN 
                       ('New', 'In Cart', 'Checkout', 'Paid', 'Shipped') 
                      ) NOT NULL,
	order_created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE order_items (
	order_id INT references orders,
	product_id INT references products,
	quantity INT DEFAULT 1 CHECK ( quantity > 0 ),
	PRIMARY KEY (order_id, product_id)
);


/*

	ADD VALUES TO THE TABLES

*/
INSERT INTO countries VALUES ('az', 'Azerbaijan', 'Asia'), 
                             ('bb', 'Barbados', 'North America'), 
                             ('bd', 'Bangladesh', 'Asia'), 
                             ('be', 'Belgium', 'Europe'), 
                             ('bm', 'Bermuda', 'North America'), 
                             ('bs', 'Bahamas', 'North America'), 
                             ('ca', 'Canada', 'North America'), 
                             ('cn', 'China', 'Asia'), 
                             ('cu', 'Cuba', 'North America'), 
                             ('de', 'Germany', 'Europe'), 
                             ('fr', 'France', 'Europe'), 
                             ('ht', 'Haiti', 'North America'), 
                             ('id', 'Indonesia', 'Asia'), 
                             ('in', 'India', 'Asia'), 
                             ('iq', 'Iraq', 'Asia'), 
                             ('ir', 'Iran', 'Asia'), 
                             ('jm', 'Jamaica', 'North America'), 
                             ('jp', 'Japan', 'Asia'), 
                             ('mx', 'Mexico', 'North America'), 
                             ('ni', 'Nicaragua', 'North America'), 
                             ('nl', 'Netherlands', 'Europe'), 
                             ('pa', 'Panama', 'North America'),
                             ('us', 'USA', 'North America');
                             
INSERT INTO users(user_id, user_full_name) VALUES 
						 (1, 'Hedvig Madhu'), 
                         (2, 'Casandra Walton'), 
                         (3, 'Alexander Lovisa'), 
                         (4, 'Chiyoko Kenji'), 
                         (5, 'Rene Goscinny'), 
                         (6, 'Alfred Neuwald'), 
                         (7, 'Bill Watterson');
                         
INSERT INTO products (product_id, product_name, product_price, product_type) VALUES 
	(1, 'Isnogud 50 Year Edition', 139, 'Comic'),
	(2, 'Karl der Kleine in der Klimahölle', 12.9, 'Comic'),
    (3, 'Karl der Kleine - Die Stadt der Printen', 11.5, 'Comic'),
    (4, 'Calvin und Hobbes 6: Wissenschaftlicher Fortschritt macht "Boing"', 12.9, 'Comic');
    
INSERT INTO stores(store_id, store_name, store_country, admin_id) VALUES 
	(1, 'Comic Gallery', 'de', 4),
    (2, 'Atomik Stripwinkel', 'nl', 1),
    (3, 'Midtown Comics Times Square', 'us', 2 ),
	(4, 'Comic Toranoana Akihabara Shop', 'jp', 3 );
    
INSERT INTO orders VALUES
	(1,3,3,'In Cart','2019-8-26 4:58:11+01'),
	(2,4,2,'Paid','2019-1-2 19:33:55+01'),
	(3,3,4,'Paid','2019-9-16 3:42:7+01'),
	(4,2,3,'Paid','2019-1-14 10:22:26+01'),
	(5,2,2,'Shipped','2019-12-14 7:38:20+01'),
	(6,1,5,'Paid','2019-11-19 6:45:41+01'),
	(7,2,2,'Paid','2020-4-5 9:55:54+01'),
	(8,3,6,'Paid','2020-8-3 1:12:9+01'),
	(9,1,1,'In Cart','2020-3-7 6:40:37+01'),
	(10,4,4,'Shipped','2020-8-16 10:31:23+01'),
	(11,2,2,'Shipped','2020-6-2 16:59:10+01'),
	(12,2,2,'In Cart','2021-7-2 8:23:33+01'),
	(13,3,4,'Paid','2021-7-7 4:14:59+01'),
	(14,1,5,'Shipped','2021-9-16 5:22:52+01'),
	(15,4,3,'Paid','2021-9-5 20:45:29+01');


INSERT INTO order_items VALUES 
	(1,3,16),
	(2,1,6),
	(3,4,26),
	(4,2,63),
	(5,1,38),
	(6,2,32),
	(7,2,72),
	(8,3,15),
	(9,2,48),
	(10,1,70),
	(11,1,41),
	(12,1,80),
	(13,3,91),
	(14,2,20),
	(15,3,6),
    (16,4,6);
                         

